local thisDir = debug.getinfo(2, "S").source:sub(2):match('.*/') or ''
dofile(thisDir..'pc/PowerChat.lua')

if not PowerChat then
    print('Could not load PowerChat')
    io.read()
    return
end


local po = print

print = function(str, ...)
    str = str:gsub('\n', '\n ')

    po(' '..str, ...)
end

math.randomseed(os.time())

PowerChat:Monitor(
    PowerChat:NewInstance(tostring(os.time()), print, nil, thisDir..'cmd/'),
    function() return io.read() end,
    print
)
