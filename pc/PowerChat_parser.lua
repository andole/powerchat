local PC = PowerChat.PC
local unpack = unpack or table.unpack
local loadstring = loadstring or load


function PC:Set(left, right)
    local tbl, name = self:ResolveNamespace(left, true)
    left = name or left
    tbl[left] = right
end


function PC:DoCmd(cmd, args, ops, raw)
    local tbl
    local c
    local external
    local external_name

    c, external, external_name, tbl = self:ResolveNamespace(cmd)

    if not c then
        tbl = self.namespace
        c = self.namespace[cmd]
    end


    if c then
        cmd = external_name or cmd
        local arg_name = cmd..'.arg'
        local opt_name = cmd..'.opt'

        for k,v in ipairs(args) do
            tbl[arg_name..k] = v
        end

        for k,v in ipairs(ops) do
            tbl[opt_name..k] = v
        end

        tbl[arg_name..'-1'] = tostring(#args)
        tbl[opt_name..'-1'] = tostring(#ops)

        local sym = self.parser.syms.cmd

        if type(c) == 'string' then
            local arg0 = tbl[cmd..'._arg0'] or cmd
            c = c:gsub(sym..(tbl[arg0..'._arg0_name'] or '0'), arg0)

            c = c:gsub(sym..'(%d+)', function(i) return args[tonumber(i)] or '' end)

            c = c:gsub(sym..'%.(%d+)', function(i) return ops[tonumber(i)] or '' end)

            c = c:gsub(sym..'raw', raw)

            if c:match(self.parser.regexes.belongs) then
                local syms = nil
                if sym ~= self.parser.default_cmd then
                    syms = { [2] = sym }
                end

                local p = PowerChat.PCp:new(function(...) return self:DoCmd(...) end,
                                    function(...) return self:Set(...) end,
                                    syms,
                                    PC.DEBUG and function(...) self:Out(...) end or nil)
                c = p:Process(c)
            end

            return c
        else
            local f_ret, stop
            if external then
                self.enabled = false
                f_ret, stop = c(cmd, external, args, ops, raw, self.output, self)
                self.enabled = true
            else
                f_ret, stop = c(args, ops, raw)
            end

            return f_ret or ''
        end
    end

    return ''
end


function PC:InitParser(cmd_sym)
    if cmd_sym then cmd_sym = self:UM(cmd_sym) end

    self.parser = {}

    self.parser.default_cmd = '!'

    self.parser.syms = {
        cmd = cmd_sym or self.parser.default_cmd,
        internal = '№',
        block_start = '{',
        block_fin = '}',
        set = '=',
        conj = '&',
        opt = '-',
        exec_set = ':',
        comment = '#',
        nl_block_start = '%[',
        nl_block_end = '%]'
    }

    local s = self.parser.syms

    local blk = s.block_start..s.block_fin
    local nl_blk = s.nl_block_start..s.nl_block_end

    self.parser.regexes = {
        belongs = '['..blk..s.cmd..s.internal..']',
        newline_block = s.nl_block_start..'([^'..nl_blk..']*\n[^'..nl_blk..']*)'..s.nl_block_end,
        disable = self.parser.syms.cmd..'disable',
        enable = self.parser.syms.cmd..'enable',
        external = '^(.+)%->(.+)$'
    }

    self.parser.regexes_logic = {
        newline_block = {func = function(self, raw, capture)
                                   return capture:gsub('%s*\n%s*','')
                                end }
    }
end


function PC:ResolveNamespace(str, gonna_set)
    if not str then return end

    local ext, name = str:match(self.parser.regexes.external)

    local c
    local external
    local external_name
    local tbl

    if ext and name then
        for k,v in ipairs(PC.external) do
            if v.id == ext then
                if gonna_set then return PC.external[k].commands, name end

                if v.commands[name] then
                    c = v.handler or v.commands[name]
                    external = v.handler and v.commands[name] or nil
                    external_name = name
                    tbl = PC.external[k].commands
                    if type(external) == 'string' then
                        c = external
                        external = false
                    end
                    return c, external, external_name, tbl
                end
            end
        end
        if gonna_set then
            table.insert(PC.external, {id = ext, commands = {}})
            return PC.external[#PC.external].commands, name
        end

        if PC.DEBUG then
            self:Out('Variable or namespace not found: '..str)
        end

        return
    end

    if gonna_set then return self.namespace, str end

    if self.namespace[str] then
        return self.namespace[str], false, false, self.namespace
    end

    if self.commands[str] then
        return self.commands[str], false, false, self.commands
    end

    if self.namespace['_sandbox'] == 'true' then
        return
    end

    for k,v in ipairs(PC.external) do
        if v.commands[str] then
            c = v.handler or v.commands[str]
            external = v.handler and v.commands[str] or nil
            external_name = str
            tbl = PC.external[k].commands
            if type(external) == 'string' then
                c = external
                external = false
            end
            return c, external, external_name, tbl
        end
    end

    if PC.DEBUG then
        self:Out('Variable not found: '..str)
    end
end


function PC:Process(str, sym)

    if str == '' then return str end

    if sym then
        local inst = self.instances[self:UM(sym)]

        if inst then
            return inst:Process(str)
        end
    end

    for k,v in pairs(self.instances) do
        if str:match(k) then
            return v:Process(str)
        end
    end

    if str:match(self.parser.regexes.belongs) then

        if str == self.parser.regexes.disable and self.enabled then
            self.enabled = false
            self:Out('PowerChat ('..self.parser.syms.cmd..') disabled')
            return ''
        elseif str == self.parser.regexes.enable and not self.enabled then
            self.enabled = true
            self:Out('PowerChat ('..self.parser.syms.cmd..') enabled')
            return ''
        end

        if not self.enabled then return str end

        if PC.LOG then
            self:Log(str)
        end

        local syms = nil
        if (sym or self.parser.syms.cmd) ~= self.parser.default_cmd then
            syms = { [2] = sym or self.parser.syms.cmd }
        end
        local p = PowerChat.PCp:new(function(...) return self:DoCmd(...) end,
                                    function(...) return self:Set(...) end,
                                    syms,
                                    PC.DEBUG and function(...) self:Out(...) end or nil)
        local success, newstr = pcall(p.Process,p,str)

        if success then
            if newstr then
                if newstr == str then
                    self.last_msg = newstr
                    return '', true
                else
                    if #self.history == 20 then
                        table.remove(self.history)
                    end
                    table.insert(self.history, 1, str)

                    if newstr~='' and newstr:sub(1,1) == self.parser.syms.conj then
                        newstr = newstr:sub(2)
                        for k,v in pairs(self.parser.syms) do
                            local repl = v:gsub('%%','')
                            newstr = newstr:gsub('__'..k, repl)
                        end
                        newstr = newstr:gsub('__(%d+)', function(c) return string.char(tonumber(c)) end)
                    end

                    newstr = newstr:gsub('\\n','\n')

                    if self.subinstance and newstr~='' then
                        newstr = '['..self.parser.syms.cmd:gsub('%%','')..'] '..newstr
                    end

                    if newstr:match('^%s*$') then
                        return ''
                    end

                    if PC.LOG then
                        self:Log(newstr)
                    end

                    return newstr, true
                end
            end
        else
            self:Out(newstr)
        end
    else
        self.last_msg = str
        return '', true
    end

    return ''
end
