-- Debug tools
local DEBUG = false             -- If true, debug messages are sent to output
local LOG = false               -- If PC should log output, pc_log.txt


local thisDir, thisPath
local function Dirs()
    thisPath = debug.getinfo(2, "S").source:sub(2)
    thisDir = string.match(thisPath, '.*/') or ''
end
Dirs()
Dirs = nil

local unpack = unpack or table.unpack
local loadstring = loadstring or load

local class
local file = io.open(thisDir..'class.lua', 'r')
if file then
    local data = file:read('*all')
    file:close()

    class = loadstring(data)()
else
    class = class
end


PowerChat = PowerChat or {}
PowerChat.class = class

local PC = class()

PC.thisDir = thisDir
PC.DEBUG = DEBUG
PC.LOG = LOG

PowerChat.PC = PC

local instances = {}
local command_files = {}


PC.external = {}
function PowerChat:AddExternal(id,cmd_table, handler_func)
    if type(cmd_table) ~= 'table' then
        return
    end

    if PC.DEBUG then
        local any_inst = next(instances)
        if any_inst then
            any_inst:Out('Adding external: '..id)
        end
    end

    table.insert(PC.external, {id = id, commands = cmd_table, handler = handler_func})

    return true
end


function PowerChat:AddCommandsFile(filename, not_for_existing)
    if type(filename) ~= 'string'
        or filename == ''
        or filename:match('^%s+$')
    then
        return false
    end

    if not not_for_existing then
        for k,v in pairs(instances) do
            v.commands.exec({filename},{})
        end
    end

    table.insert(command_files, filename)

    return true
end


function PowerChat:NewInstance(id, ...)
    if not id or instances[id] then return end

    local ni = PC:new(...)

    for k,v in ipairs(command_files) do
        ni.commands.exec({v},{})
    end

    instances[id] = ni

    return ni
end


local preload = { 'PowerChat_parser.lua',
                'PowerChat_commands.lua',
                'PowerChat_parser2.lua' }
for k,v in ipairs(preload) do
    dofile(thisDir..v)
end


function PC:Out(str, ...)
    str = tostring(str)

    if str == '' then return end

    if str:sub(1,1) == self.parser.syms.conj then
        str = str:sub(2)
        for k,v in pairs(self.parser.syms) do
            local repl = v:gsub('%%','')
            str = str:gsub('__'..k, repl)
        end
        str = str:gsub('__(%d+)', function(c) return string.char(tonumber(c)) end)
    end

    str = str:gsub('\\n','\n')

    if self.subinstance then
        local prefix = '^%['..self.parser.syms.cmd..'%]'
        if not str:match(prefix) then
            str = '['..self.parser.syms.cmd:gsub('%%','')..'] '..str
        end
    end

    if PC.LOG then
        self:Log(str)
    end

    self.output(str, ...)
end


function PC:UM(str)
    local ret = str:gsub('[.+[()$%]%-%^%%?*]','%%%1')
    return ret
end


function PC:init(output, sym, cmd_path)
    local start_time = os.clock()

    self.cmd_path = cmd_path

    self.init_done = self.init_done or false
    self.to_hash = ''

    self:InitCommands()

    self.output = output

    self:InitParser(sym)

    self.instances = self.instances or {}

    self.enabled = true

    self.history = {}
    self.namespace = self.commands

    if not (self.cmd_path and self:Exec(self.cmd_path..'commands.pc', sym)) then
        if not self:Exec(thisDir..'commands.pc', sym)
           and not self:Exec('commands.pc', sym) then
            self:Out('Could not open the file with base commands')
        end
    end

    self:SelfHash()

    if self.hash and not self.init_done then
        self.commands['powerchat.hash'] = self.hash
        local str = '### PowerChat by andole ### Hash: ' .. self.hash
        str = str .. ' ### Startup time: '..(os.clock()-start_time)..'s ###\n'
        self:Out(str)
    end

    self.init_done = true
    self.last_msg = nil
end

-- Good enough
function PC:Hash(str)
    local hash = 0
    for i = 1, str:len() do
        hash = hash + string.byte(str, i) + i
    end

    local ret = tostring(hash)
    return ret:sub(math.max(ret:len()-6, 1), ret:len())
end


function PC:SelfHash()
    if self.parser.default_cmd ~= self.parser.syms.cmd then
        self.hash = nil
        return
    end

    local function r(filename)
        local file = io.open(filename, 'r')
        if file then
            local data = file:read('*all')
            file:close()
            return data
        end
        self:Out('Could not open '..filename)
        return ''
    end

    for k,v in ipairs(preload) do
        self.to_hash = self.to_hash..r(thisDir..v)
    end

    self.to_hash = self.to_hash..r(thisPath)

    self.hash = self:Hash(self.to_hash)
end


function PC:Exec(filename, sym)
    sym = sym or self.parser.syms.cmd

    local file = io.open(filename, 'r')

    if file then
        local data = file:read('*all')
        file:close()

        if not self.init_done then
            self.to_hash = self.to_hash..data
        end

        if sym ~= self.parser.default_cmd then
            local repl = sym:gsub('%%','')
            data = data:gsub(self:UM(sym),
                       self.parser.syms.internal..self.parser.syms.internal)
            data = data:gsub(self.parser.default_cmd, repl)
            data = data:gsub(self.parser.syms.internal..self.parser.syms.internal,
                       self.parser.default_cmd)
        end

        local regex_name = 'newline_block'
        local regex = self.parser.regexes[regex_name]
        local regex_logic = self.parser.regexes_logic[regex_name]

        if regex and regex_logic and type(regex_logic.func)=='function' then
            local matched = 1
            while matched > 0 do
                data, matched = data:gsub(regex, function(...)
                    return regex_logic.func(self, data, ...) or ''
                end, 1)
            end
        end

        self.exec_return = nil
        for v in data:gmatch('[^\n]+') do
            if self.exec_return then break end

            local s = self:Process(v,sym)

            if s~= '' and not s:match('^%s+$') then
                self:Out(s)
            end
        end

        return true
    end

    return false
end


function PC:Log(str)
    local f = io.open('pc_log.txt', "a")
    f:write(str..'\n')
    f:close()
end


function PowerChat:Monitor(pc, input, output)
    while true do
        local i = input()

        if i == 'quit' or i == 'exit' then return end

        local s = pc:Process(i)

        if s ~= '' then
            output(s)
        end
    end
end
