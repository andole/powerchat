local loadstring = loadstring or load

local PCp = PowerChat.class()
PowerChat.PCp = PCp

local NO_TYPE = 1
local CMD = 2
local EXEC_SET = 3
local SET = 4
local BL_ST = 5
local BL_FIN = 6
local NBL_ST = 7
local NBL_FIN = 8
local CMNT = 9
local OPT = 10
local CONJ = 11
local SPACE = 12
local TEXT = 13

local token_types = {
    ['!'] = CMD,
    [':'] = EXEC_SET,
    ['='] = SET,
    ['{'] = BL_ST,
    ['}'] = BL_FIN,
    ['['] = NBL_ST,
    [']'] = NBL_FIN,
    ['#'] = CMNT,
    --['.'] = 'sep',
    ['-'] = OPT,
    ['&'] = CONJ,
    [' '] = SPACE
}


local funcs = {
    [CMD] = 'pCmd',
    [BL_ST] = 'pBlock',
    [NBL_ST] = 'pNewBlock',
    [CMNT] = 'pComment'
}


function PCp:init(do_func, set_func, syms, error_func)
    self.DoCmd = do_func
    self.Set = set_func
    self.Error = error_func
    self.token_types = token_types
    if syms then self:SetSyms(syms) end
end


function PCp:SetSyms(syms)
    local new_types = {}
    for k,v in pairs(token_types) do
        if syms[v] then
            new_types[syms[v]] = v
        else
            new_types[k] = v
        end
    end
    self.token_types = new_types
end


function PCp:pWhatIsThis(str)
    return self.token_types[str] or TEXT
end


function PCp:pIsMath(str)
    if str:match('^[%d%-]+[%.%+%-%*%%%/][%.%d%+%-%*%%%/]+$') then
        local num = loadstring('return '..str)

        if not num then
            if self.Error then
                self.Error('Wrong math format: '..str)
            end
            return str
        end

        num = num()

        if num then
            return tostring(num)
        end

        self.Error('Wrong math format (2): '..str)
    end

    return str
end


function PCp:pGetTokens(str)
    local ar = {}
    for i=1, str:len() do
        ar[i] = str:sub(i,i)
    end

    local tokens = {}
    local prev = NO_TYPE
    local text = ''
    local inx = 1
    for i=1, #ar do
        local token_type = self:pWhatIsThis(ar[i])
        if not (token_type == SPACE and (prev == SPACE or prev == BL_ST)) then

            if token_type == OPT and prev ~= SPACE then
                token_type = TEXT
            end

            if token_type == TEXT then
                text = text .. ar[i]
            else
                if text ~= '' then
                    text = self:pIsMath(text) or text
                    tokens[inx] = { t = TEXT, c = text:gsub('^%s*(.-)%s*$', '%1') }
                    inx = inx + 1
                    text = ''
                end

                tokens[inx] = { t = token_type, c = ar[i] }

                inx = inx + 1
            end

            prev = token_type
        end
    end

    if text ~= '' then
        tokens[inx] = { t = TEXT, c = text:gsub('^%s*(.-)%s*$', '%1') }
        inx = inx + 1
    end

    return tokens
end


function PCp:pNextToken()
    self.p.inx = self.p.inx + 1
    self.p.prev2 = self.p.prev or {t=NO_TYPE,c=''}
    self.p.prev = self.p.cur or {t=NO_TYPE,c=''}
    self.p.cur = self.p.tokens[self.p.inx]
    return self.p.cur
end


function PCp:pBack()
    self.p.inx = self.p.inx - 1
    self.p.cur = self.p.prev
    self.p.prev = self.p.prev2

    return self.p.cur
end


function PCp:Process(str)
    return self:pParse(self:pGetTokens(str))
end


function PCp:pParse(tokens)
    self.p = {inx=0}
    self.p.tokens = tokens

    local newstr = ''
    while self:pNextToken() do
        local current = self.p.cur

        if funcs[current.t] then
            newstr = newstr .. self[funcs[current.t]](self)
        else
            newstr = newstr .. current.c
        end
    end

    return newstr
end


function PCp:pCmd()
    local start_of_block = self.p.prev.t == BL_ST or self.p.prev.t == NO_TYPE
    if not start_of_block and self.p.prev.t ~= SPACE then
        return ''
    end

    local cmds = {}
    local ops = {}
    local args = {}
    local newstr = ''

    while self:pNextToken() do
        local current = self.p.cur

        if funcs[current.t] then
            newstr = newstr .. self[funcs[current.t]](self, newstr)
        elseif current.t == SET or current.t == EXEC_SET then
            self:pSet(newstr)
            return ''
        elseif current.t == CONJ then
            table.insert(cmds, newstr)
            newstr = ''
        elseif current.t == SPACE then
            break
        elseif self.p.in_block and current.t == BL_FIN then
            if start_of_block then
                self:pBack()
            end
            break
        else
            newstr = newstr .. current.c
        end
    end

    table.insert(cmds, newstr)

    if not start_of_block then
        local ret = ''
        for k,v in ipairs(cmds) do
            ret = ret..string.gsub(self.DoCmd(v,{},{},'') or '','^%s*(.-)%s*$', '%1')
        end

        self:pBack()

        return ret
    end

    newstr = ''
    local isopt = false
    local isblock = false
    local iscmd = false
    local raw = ''

    while self:pNextToken() do
        local current = self.p.cur

        if funcs[current.t] then
            local res = self[funcs[current.t]](self)

            if current.t == BL_ST then
                isblock = true
            elseif current.t == CMD then
                iscmd = true
            end

            newstr = newstr .. res
            raw = raw .. res
        elseif current.t == OPT and self.p.prev.t == SPACE then
            isopt = true
            raw = raw .. current.c
        elseif current.t == SPACE then
            if isopt then
                table.insert(ops, newstr)
                isopt = false
            elseif newstr ~= '' then
                if isblock or iscmd then
                    isblock = false
                    iscmd = false
                    for v in newstr:gmatch('%S+') do
                        if self:pWhatIsThis(v:sub(1,1)) == OPT then
                            table.insert(ops, v:sub(2))
                        else
                            table.insert(args, v)
                        end
                    end
                else
                    table.insert(args, newstr)
                end
            end
            newstr = ''
            raw = raw .. current.c
        elseif self.p.in_block and current.t == BL_FIN then
            self:pBack()
            break
        else
            newstr = newstr .. current.c
            raw = raw .. current.c
        end
    end

    if isopt then
        table.insert(ops, newstr)
    elseif newstr ~= '' then
        if isblock or iscmd then
            for v in newstr:gmatch('%S+') do
                if self:pWhatIsThis(v:sub(1,1)) == OPT then
                    table.insert(ops, v:sub(2))
                else
                    table.insert(args, v)
                end
            end
        else
            table.insert(args, newstr)
        end
    end

    local ret = ''
    for k,v in ipairs(cmds) do
        ret = ret..string.gsub(self.DoCmd(v,args,ops,raw) or '','^%s*(.-)%s*$', '%1')
    end

    return ret
end


function PCp:pSet(str)
    local set_type = self.p.cur.t

    local val = self:pNextToken()

    if not val then
        self.Set(str, '')
        return ''
    end

    if val.t == BL_ST then
        if set_type == SET then
            val = self:pGetBlock()
        else
            val = self:pBlock()
        end
    elseif self.p.cur.t == NBL_ST then
        val = self:pNewBlock()
    else
        val = val.c
        while self:pNextToken() do
            if self.p.cur.t == BL_ST then
                val = val .. self:pBlock()
            elseif self.p.cur.t == NBL_ST then
                val = val .. self:pNewBlock()
            elseif self.p.cur.t == BL_FIN then
                self:pBack()
                break
            elseif self.p.cur.t == SPACE then
                self:pBack()
                break
            else
                val = val .. self.p.cur.c
            end
        end
    end


    self.Set(str, val)

    return ''
end


function PCp:pGetBlock()
    local level = 1
    local ret = ''
    local prev = self.p.prev.c

    while self:pNextToken() do
        if self.p.cur.t == BL_ST then
            level = level + 1
            ret = ret .. self.p.cur.c
        elseif self.p.cur.t == BL_FIN then
            level = level - 1
            if level == 0 then
                return ret
            else
                ret = ret .. self.p.cur.c
            end
        else
            ret = ret .. self.p.cur.c
        end
    end

    if self.Error then
        prev = prev == '' and '<beginning of the line>' or prev
        self.Error('No closing character for the block that starts after '..prev)
    end

    return ret
end


function PCp:pBlock()
    self.p.in_block = true
    self.p.in_block_cnt = (self.p.in_block_cnt or 0) + 1
    local prev = self.p.prev.c

    local newstr = ''
    while self:pNextToken() do
        local current = self.p.cur

        if current.t == BL_FIN then
            self.p.in_block_cnt = self.p.in_block_cnt - 1
            if self.p.in_block_cnt == 0 then
                self.p.in_block = false
            end

            return self:pIsMath(newstr)
        elseif funcs[current.t] then
            local r = self[funcs[current.t]](self)

            newstr = newstr .. r
        else
            newstr = newstr .. current.c
        end
    end

    if self.Error then
        prev = prev == '' and '<beginning of the line>' or prev
        self.Error('No closing character for the block that starts after '..prev)
    end

    return self:pIsMath(newstr)
end


function PCp:pNewBlock()
    local level = 1
    local ret = ''
    local prev = self.p.prev.c

    while self:pNextToken() do
        if self.p.cur.t == NBL_ST then
            level = level + 1
            ret = ret .. self.p.cur.c
        elseif self.p.cur.t == NBL_FIN then
            level = level - 1
            if level == 0 then
                return ret
            else
                ret = ret .. self.p.cur.c
            end
        else
            ret = ret .. self.p.cur.c
        end
    end

    if self.Error then
        prev = prev == '' and '<beginning of the line>' or prev
        self.Error('No closing character for the block that starts after '..prev)
    end

    return ret
end


function PCp:pComment()
    while self:pNextToken() do
        if self.p.cur.t == CMNT then
            return ''
        end
    end
    return ''
end
