local loadstring = loadstring or load

local PC = PowerChat.PC
local thisDir = PC.thisDir

function PC:InitCommands()
    self.commands = {}

    self.commands.lua = function(args, ops, raw)
        local ls = loadstring(raw)
        if ls then return tostring(ls() or '') end
    end

    self.commands['return'] = function(args, ops, raw)
        self.exec_return = raw
        return '', true
    end

    self.commands['debug'] = function(args, ops, raw)
        PC.DEBUG = not PC.DEBUG
        self:Out('Debug messages are now '..(PC.DEBUG and 'enabled' or 'disabled'))
    end

    self.commands['powerchat.dump'] = function(args, ops, raw)
        local file = io.open(args[1], 'a')

        local keys = {}
        for k,v in pairs(self.namespace) do
            table.insert(keys, k)
        end
        table.sort(keys, function(a,b) return a<b end)

        if file then
            file:write('----------------------------\n')
            for k,v in ipairs(keys) do
                local vv = self.namespace[v]
                if type(vv) == 'string' then
                    file:write(v..' = '..vv..'\n')
                end
            end
            file:close()
        else
            self:Out('Could not open/create the file: '..args[1])
        end
    end

    self.commands.rep = function(args, ops, raw)
        if self.last_msg then
            self:Out(self.last_msg)
        end
    end

    self.commands.crep = function(args, ops, raw)
        local cmd = self.history[1]

        if cmd:match(self.parser.syms.cmd..'crep') then
            cmd = self.history[2]
            table.remove(self.history, 1)
        end

        local ret = self:Process(cmd)
        if ret~='' then
            self:Out(ret)
        end
    end

    self.commands.um = function(args,ops,raw)
        return self:UM(raw)
    end

    self.commands.nospaces = function(args,ops,raw)
        local ret = raw:gsub('%s', '')
        return ret
    end

    self.commands.out = function(args, ops, raw)
        self:Out(raw)
    end

    self.commands['powerchat.reset'] = function()
        self:init(self.output, nil, self.cmd_path)
        self:Out('Reset to defaults')
    end

    self.commands['for'] = function(args,ops,raw)
        local body_name = args[2] or args[1]
        local from, to, step = tonumber(ops[1]), tonumber(ops[2]), (tonumber(ops[3]) or 1)

        if from and to and body_name then
            for i=from,to,step do
                local arg = tostring(i)
                self:DoCmd(body_name, {arg}, {}, arg)
            end
        end

        return ''
    end

    self.commands.isdef = function(args, ops, raw)
        local var, opt1, opt2 = args[1], ops[1], ops[2]

        if var and opt1 and opt2 then
            local c, ext, ext_name, tbl = self:ResolveNamespace(var)
            c = ext or c
            if c then
                return opt1
            else
                return opt2
            end
        else
            self:Out('Wrong usage of \'isdef\', example: !isdef var1 -yes -no')
        end
    end

    self.commands.get = function(args, ops, raw)
        local c, ext, ext_name, tbl = self:ResolveNamespace(args[1])
        local cmd = ext or c

        if type(cmd) ~= 'string' then return end

        return cmd, true
    end

    self.commands.namespace = function(args, ops, raw)
        if args[1] and not args[1]:match('^%s*$') then

            for k,v in ipairs(PC.external) do
                if v.id == args[1] then
                    self.namespace = PC.external[k].commands
                    return
                end
            end

            table.insert(PC.external, {id = args[1], commands = {}})

            self.namespace = PC.external[#PC.external].commands
            return
        end

        self.namespace = self.commands
    end

    self.commands['namespace-clear'] = function(args, ops, raw)
        if args[1] and not args[1]:match('^%s*$') then
            for k,v in ipairs(PC.external) do
                if v.id == args[1] then
                    PC.external[k].commands = {}
                    return
                end
            end
        end
    end

    self.commands['tochararray'] = function(args, ops, raw)
        if not (args[1] and args[2]) then return end

        local c, ext, ext_name, tbl = self:ResolveNamespace(args[1])
        local str = ext or c

        if type(str) ~= 'string' then return end

        local tbl2, name = self:ResolveNamespace(args[2], true)
        local sep = args[3] or '.'
        for i=1,str:len() do
            tbl2[(name or args[2])..sep..i] = str:sub(i,i)
        end
        tbl2[(name or args[2])..sep..'-1'] = tostring(str:len())
    end

    self.commands['tostring'] = function(args, ops, raw)
        if not (args[1] and args[2]) then return end

        local sep = args[3] or '.'

        local c, ext, ext_name, tbl = self:ResolveNamespace(args[1]..sep..'1')
        local str = ext or c

        if type(str) ~= 'string' then return end

        local tbl2, name = self:ResolveNamespace(args[2], true)

        local count = tonumber(tbl[args[1]..sep..'-1'])

        if not count then return end

        local res = ''
        for i=1,count do
            res = res .. (tbl[args[1]..sep..i] or '')
        end

        tbl2[name or args[2]] = res
    end

    self.commands['get-line'] = function(args, ops, raw)
        if not args[1] then return end

        local c, ext, ext_name, tbl = self:ResolveNamespace(args[1])
        local str = ext or c

        if type(str) ~= 'string' then return end

        local line = str:match('^(.-)\n') or str

        tbl[ext_name or args[1]] = str:gsub('^.-\n', '', 1)

        if args[2] then
            local tbl2, name = self:ResolveNamespace(args[2], true)
            tbl2[name or args[2]] = line
        else
            return line
        end
    end

    self.commands['type-types'] = 'empty number script string'
    self.commands.type = function(args, ops, raw)
        local c, ext, ext_name, tbl = self:ResolveNamespace(args[1])
        local str = ext or c
        str = str or ops[1]

        if type(str) == 'string' then
            if str == '' or str:match('^%s+$') then return 'empty' end

            if str:match('^%-?%d+.?%d*$') then return 'number' end

            if str:match(self.parser.regexes.belongs) then return 'script' end

            return 'string'
        end
    end

    self.commands.exec = function(args, ops, raw)
        if args[1] then
            if args[2] and self:Exec(args[2]..args[1], ops[1]) then
                return self.exec_return or ''
            end
            if self.cmd_path and self:Exec(self.cmd_path..args[1], ops[1]) then
                return self.exec_return or ''
            end
            if not self:Exec(thisDir..args[1], ops[1]) and not self:Exec(args[1], ops[1]) then
                self:Out('Could not open file: '..args[1])
            end

            return self.exec_return or ''
        end
    end

    self.commands['file-read'] = function(args, ops, raw)
        if not args[1] then
            self:Out('Specify filename')
            return
        end

        if not args[2] then
            self:Out('Specify variable')
            return
        end

        local data
        local file = io.open(thisDir..args[1], 'r')

        if file then
            data = file:read('*all')
            file:close()
        else
            local file2 = io.open(args[1], 'r')

            if file2 then
                data = file2:read('*all')
                file2:close()
            else
                self:Out('Could not find the file: '..args[1])
            end
        end

        if data then
            local tbl, name = self:ResolveNamespace(args[2], true)
            args[2] = name or args[2]
            tbl[args[2]] = data
            local i = 1
            for v in data:gmatch('[^\n]*') do
                tbl[args[2]..'.'..i] = v
                i = i + 1
            end
            tbl[args[2]..'.size'] = tostring(i-1)
        end
    end

    self.commands['file-write'] = function(args, ops, raw)
        if not args[1] then
            self:Out('Specify filename')
            return
        end

        if not args[2] then
            local c, ext = self:ResolveNamespace(ops[1])
            args[2] = ext or c
        end

        if type(args[2]) ~= 'string' then
            self:Out('Specify text or a string/script variable')
            return
        end

        local file2 = io.open(args[1], 'a')

        if file2 then
            file2:write(args[2]..'\n')
            file2:close()
        else
            self:Out('Could not open/create the file: '..args[1])
        end
    end

    self.commands['file-remove'] = function(args, ops, raw)
        if not args[1] then
            self:Out('Specify filename')
            return
        end

        local file = io.open(thisDir..args[1], 'r')

        if file then
            file:close()
            os.remove(thisDir..args[1])
        else
            local file2 = io.open(args[1], 'r')

            if file2 then
                file2:close()
                os.remove(args[1])
            else
                self:Out('Could not find the file: '..args[1])
            end
        end
    end

    self.commands.copy = function(args, ops, raw)
        local from, to, sep = args[1], args[2], args[3] or ops[1] or '.'

        if from and to and sep then
            local c, ext, ext_name, tbl_from = self:ResolveNamespace(from)
            from = ext_name or from
            local tbl_to, to_name = self:ResolveNamespace(to, true)
            to = to_name or to

            if tbl_from[from] then
                tbl_to[to] = tbl_from[from]
            end

            if sep == 'false' then return end

            local r_from = '^'..self:UM(from..sep)..'(.+)'
            local r_to = to..sep

            local add = {}
            for k,v in pairs(tbl_from) do
                local name = k:match(r_from)
                if name then
                    add[r_to..name] = v
                    add[r_to..name..sep..'_arg0'] = to
                end
            end
            for k,v in pairs(add) do
                tbl_to[k] = v
            end
        else
            self:Out('Wrong usage of \'copy\', example: !copy from to [separator]')
        end
    end

    self.commands.newinstance = function(args)
        if not args[1] or args[1] == '' then
            self:Out('Specify the new control character')
            return
        end

        if args[1]:len() > 1 then
            self:Out('Must be one character')
            return
        end

        local s = self:UM(args[1])

        if self.subinstance then
            self:Out('This is a subinstance already')
            return
        end

        for k,v in pairs(self.parser.syms) do
            if s == v then
                self:Out('This character is in use already')
                return
            end
        end

        if self.instances[s] then
            self:Out('A subinstance is using this character already')
            return
        end

        self.instances[s] = PC:new(self.output, args[1], self.cmd_path)
        self.instances[s].subinstance = true

        self:Out('Subinstance created: '..args[1])
    end
end
