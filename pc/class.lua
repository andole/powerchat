local class = function()
    local c = {}

    function c:new(...)
        local o = {}
        setmetatable(o, self)
        self.__index=self

        if self.init then
            self.init(o, ...)
        end

        return o
    end

    return c
end

return class
